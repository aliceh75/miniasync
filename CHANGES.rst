Changes
=======

V0.1.2
------
- Documentation fixes

V0.1.1
--------
- Documentation improvements

V0.1.0
------
- Initial implementation: miniasync.run and miniasync.loop
