import asyncio
import miniasync

from unittest import TestCase


class TestMiniAsyncLoop(TestCase):
    def test_loop_creates_a_new_event_loop(self):
        prev_loop = asyncio.get_event_loop()
        with miniasync.loop():
            self.assertIsNot(asyncio.get_event_loop(), prev_loop)
        self.assertIs(asyncio.get_event_loop(), prev_loop)

    def test_loop_yields_miniasync_object(self):
        with miniasync.loop() as loop:
            self.assertIsInstance(loop, miniasync.MiniAsync)

    def test_loop_closes_inner_loop(self):
        with miniasync.loop():
            inner_loop = asyncio.get_event_loop()
        self.assertTrue(inner_loop.is_closed())
