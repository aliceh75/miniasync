import os
import unittest


def miniasync_test_suite():
    test_loader = unittest.TestLoader()
    test_suite = test_loader.discover(os.path.dirname(__file__), pattern='test_*.py')
    return test_suite
