import asyncio
import miniasync

from unittest import TestCase


class TestException(Exception):
    pass


class TestException2(Exception):
    pass


class TestMiniAsyncRun(TestCase):
    def test_run_invokes_all_coroutines(self):
        count = 0

        async def coro():
            nonlocal count
            count = count + 1

        with miniasync.loop() as loop:
            loop.run(
                coro(), coro(), coro()
            )

        self.assertEqual(count, 3)

    def test_run_invokes_coroutines_asynchronously(self):
        # Comunicate between two coroutines using two queues to ensure they are run asynchronously
        async def coro1(q1, q2):
            q1.put_nowait(10)
            value = await q2.get()
            q1.put_nowait(value * 2)

        async def coro2(q1, q2):
            value = await q1.get()
            q2.put_nowait(value + 1)

        with miniasync.loop() as loop:
            q1 = asyncio.Queue()
            q2 = asyncio.Queue()
            loop.run(
                coro1(q1, q2), coro2(q1, q2),
            )
            self.assertEqual(q1.get_nowait(), 22)

    def test_run_returns_values_returned_by_coroutines(self):
        async def coro(arg):
            return arg

        with miniasync.loop() as loop:
            result = loop.run(coro('hello'), coro('funny'), coro('world'))

        self.assertEqual(result, ['hello', 'funny', 'world'])

    def test_run_returns_values_in_invocation_order_not_return_order(self):
        return_order = []

        async def coro_wait(arg, q):
            nonlocal return_order
            await q.get()
            return_order.append(arg)
            return arg

        async def coro_put_nowait(arg, q):
            nonlocal return_order
            q.put_nowait(0)
            return_order.append(arg)
            return arg

        with miniasync.loop() as loop:
            q = asyncio.Queue()
            result = loop.run(
                coro_wait('hello', q), coro_put_nowait('world', q),
            )

        self.assertEqual(return_order, ['world', 'hello'])
        self.assertEqual(result, ['hello', 'world'])

    def test_run_raises_exceptions_from_coroutines_by_default(self):
        async def coro():
            raise TestException()

        with miniasync.loop() as loop:
            with self.assertRaises(TestException):
                loop.run(coro())

    def test_run_cancels_other_coroutines_when_exception_is_raised(self):
        cancelled = False

        async def coro1(q):
            nonlocal cancelled
            try:
                await q.get()
            except asyncio.CancelledError as e:
                cancelled = True
                raise e

        async def coro2():
            raise TestException()

        with miniasync.loop() as loop:
            q = asyncio.Queue()

            with self.assertRaises(TestException):
                loop.run(coro1(q), coro2())

        self.assertTrue(cancelled)

    def test_run_returns_exceptions_listed_in_return_exceptions(self):
        async def coro1():
            raise TestException()

        with miniasync.loop() as loop:
            results = loop.run(coro1(), return_exceptions=(TestException,))

        self.assertEqual(len(results), 1)
        self.assertIsInstance(results[0], TestException)

    def test_run_continues_running_after_ackowledged_exception_and_returns_all_results(self):
        async def coro1(q, value):
            await q.get()
            return value

        async def coro2(q1, q2):
            q1.put_nowait(0)
            q2.put_nowait(0)
            raise TestException()

        with miniasync.loop() as loop:
            q1 = asyncio.Queue()
            q2 = asyncio.Queue()
            results = loop.run(
                coro1(q1, 'hello'),
                coro2(q1, q2),
                coro1(q2, 'world'),
                return_exceptions=(TestException,)
            )

        self.assertEqual(len(results), 3)
        self.assertEqual(results[0], 'hello')
        self.assertIsInstance(results[1], TestException)
        self.assertEqual(results[2], 'world')

    def test_run_returns_all_acknowledged_exceptions(self):
        async def coro1():
            raise TestException()

        async def coro2():
            raise TestException2()

        async def coro3():
            return 'hello'

        with miniasync.loop() as loop:
            results = loop.run(
                coro1(), coro2(), coro3(),
                return_exceptions=(TestException, TestException2,)
            )

        self.assertEqual(len(results), 3)
        self.assertIsInstance(results[0], TestException)
        self.assertIsInstance(results[1], TestException2)
        self.assertEqual(results[2], 'hello')


class TestRun(TestCase):
    """ miniasync.run uses MiniAsync internally, so most tests are
    done on TestMiniAsyncRun. It would actually be more difficult to
    test the standalone miniasync.run, as we don't have access to the
    loop when writing the test.

    We just test enough here to ensure it's invoking miniasync.loop and
    MiniAsync properly.

    If implementation of standalone miniasync.run and MiniAsync
    were to diverge, we'd need to implement more tests here
    """
    def test_run_invokes_all_coroutines_and_returns_results(self):
        count = 0

        async def coro():
            nonlocal count
            count = count + 1
            return count

        results = miniasync.run(coro(), coro(), coro())

        self.assertEqual(count, 3)
        self.assertEqual(results, [1, 2, 3])

    def test_run_creates_inner_loop_and_closes_it(self):
        inner_loop = None
        outer_loop = asyncio.get_event_loop()

        async def coro():
            nonlocal inner_loop
            inner_loop = asyncio.get_event_loop()

        miniasync.run(coro())

        self.assertIsNot(inner_loop, outer_loop)
        self.assertTrue(inner_loop.is_closed())
