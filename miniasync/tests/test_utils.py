import asyncio

from miniasync.miniasync import _cancel_futures, SplitFutures
from unittest import TestCase


def _run_and_interupt_futures(futures):
    """ Give futures a chance to run, and then interupt them
    by raising an exception.

    This relies on the undocumented behaviour of asyncio.gather
    which will schedule the futures in the order they are provided.
    """
    class PauseException(Exception):
        pass

    async def raise_coro():
        raise PauseException()

    the_loop = asyncio.get_event_loop()
    new_futures = futures + [asyncio.ensure_future(raise_coro())]

    combined = asyncio.gather(*new_futures)
    try:
        the_loop.run_until_complete(combined)
    except PauseException:
        pass


class TestCancelFutures(TestCase):
    def test_cancel_futures_sets_futures_as_cancelled(self):
        q = asyncio.Queue()

        async def coro(q):
            await q.get()

        futures = [asyncio.ensure_future(coro(q)) for i in range(5)]
        _run_and_interupt_futures(futures)
        _cancel_futures(futures)

        # The coroutine never finishes, so all futures should be set as cancelled
        for fut in futures:
            self.assertTrue(fut.cancelled())

    def test_cancel_futures_runs_loop_after_cancelation(self):
        """ It's important the loop runs after cancel is invoked, to give
        a chance to clean up """
        cancelled = 0

        async def coro(q):
            nonlocal cancelled
            try:
                await q.get()
            except asyncio.CancelledError as e:
                cancelled += 1
                raise e

        q = asyncio.Queue()
        futures = [asyncio.ensure_future(coro(q)) for i in range(5)]
        _run_and_interupt_futures(futures)
        _cancel_futures(futures)

        # If _cancel_futures didn't run the loop after cancelation, the
        # coroutine wouldn't catch the exception as this would be 0
        self.assertEqual(cancelled, 5)


class TestSplitFutures(TestCase):
    async def coro1(self, q):
        await q.get()

    async def coro2(self, v):
        return v

    def setUp(self):
        q = asyncio.Queue()
        self.futures = [
            asyncio.ensure_future(coro) for coro in (
                self.coro1(q), self.coro2('hello'), self.coro1(q)
            )
        ]

    def tearDown(self):
        cancelled = []
        for fut in self.futures:
            if not fut.cancelled() and not fut.done():
                fut.cancel()
                cancelled.append(fut)

        if len(cancelled) > 0:
            the_loop = asyncio.get_event_loop()
            combined = asyncio.gather(*cancelled, return_exceptions=True)
            the_loop.run_until_complete(combined)

    def test_SplitFutures_returns_list_of_pending_futures(self):
        _run_and_interupt_futures(self.futures)

        split = SplitFutures(self.futures)

        not_done = [f for f in self.futures if not f.done()]
        self.assertEqual(split.pending(), not_done)

    def test_SplitFutures_merges_pending_results_correctly(self):
        _run_and_interupt_futures(self.futures)

        split = SplitFutures(self.futures)

        # Ensure we have the right pending tasks
        not_done = [f for f in self.futures if not f.done()]
        self.assertEqual(split.pending(), not_done)

        # Now test merge
        self.assertEqual(split.merge(['one', 'two']), ['one', 'hello', 'two'])
